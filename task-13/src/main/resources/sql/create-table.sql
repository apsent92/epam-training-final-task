drop table if exists books;
create table books (
bookId int AUTO_INCREMENT,
book varchar(50) NOT NULL,
primary key (bookId)
);
drop table if exists themes;
create table themes (
themeId int AUTO_INCREMENT,
theme varchar(20) NOT NULL,
primary key (themeId)
);
drop table if exists readers;
create table readers (
readerId int AUTO_INCREMENT,
name varchar(20) NOT NULL,
surname varchar(20) NOT NULL,
address varchar(40) NOT NULL,
primary key (readerId)
);
drop table if exists literature;
create table literature (
literatureId int AUTO_INCREMENT,
idBook int NOT NULL,
idTheme int NOT NULL,
cost double NOT NULL,
availability boolean NOT NULL,
primary key (literatureId),
FOREIGN KEY (idBook) REFERENCES books (bookId) 
	ON DELETE CASCADE 
	ON UPDATE CASCADE,
FOREIGN KEY (idTheme) REFERENCES themes (themeId)
	ON DELETE CASCADE 
	ON UPDATE CASCADE
);
drop table if exists library_service;
create table library_service (
idLiterature int NOT NULL,
idReader int NOT NULL,
dateIssue date NOT NULL,
dateReturn date,
delivered boolean NOT NULL,
FOREIGN KEY (idLiterature) REFERENCES literature (literatureId)
	ON DELETE CASCADE 
	ON UPDATE CASCADE,
FOREIGN KEY (idReader) REFERENCES readers (readerId)
	ON DELETE CASCADE 
	ON UPDATE CASCADE
);