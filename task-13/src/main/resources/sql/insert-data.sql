insert into books(book) values ('First Book');
insert into books(book) values ('Second Book');
insert into books(book) values ('Third Book');
insert into books(book) values ('Fourth Book');
insert into books(book) values ('Fifth Book');

insert into themes(theme) values ('Fantasy');
insert into themes(theme) values ('Detective');
insert into themes(theme) values ('Drama');
insert into themes(theme) values ('Melodrama');
insert into themes(theme) values ('Comedy');

insert into readers(name, surname, address) values ('Jon','Jonson','address1');
insert into readers(name, surname, address) values ('Kate','Read','address2');
insert into readers(name, surname, address) values ('Alex','Tate','address3');
insert into readers(name, surname, address) values ('Victor','Bush','address4');
insert into readers(name, surname, address) values ('Jane','Jameson','addres5');

insert into literature(idBook, idTheme, cost, availability) values ('1','4','30', 1);
insert into literature(idBook, idTheme, cost, availability) values ('2','3','20', 1);
insert into literature(idBook, idTheme, cost, availability) values ('3','2','25', 1);
insert into literature(idBook, idTheme, cost, availability) values ('4','5','14', 1);
insert into literature(idBook, idTheme, cost, availability) values ('5','1','17', 0);

insert into library_service(idLiterature, idReader, dateIssue, dateReturn, delivered) values ('1','1','2019-06-01','2019-07-01',1);
insert into library_service(idLiterature, idReader, dateIssue, dateReturn, delivered) values ('2','1','2019-06-12','2019-06-19',1);
insert into library_service(idLiterature, idReader, dateIssue, dateReturn, delivered) values ('3','1','2019-06-24','2019-07-08',1);
insert into library_service(idLiterature, idReader, dateIssue, dateReturn, delivered) values ('4','1','2019-07-06','2019-07-15',1);
insert into library_service(idLiterature, idReader, dateIssue, delivered) values ('5','1','2019-06-01',0);