package by.epam.training.task_13.dao;

import static by.epam.training.task_13.dao.ConstantsColumnNames.*;
import java.util.List;
import by.epam.training.task_13.model.Reader;

public interface IReaderDao {
	public static final String SQL_FIND_ALL = "select * from " + READER_TABLE;
    public static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " where " + READER_ID + " = ?";
    public static final String SQL_INSERT = "insert into " + READER_TABLE + " (" + READER_NAME + ", " + READER_SURNAME + ", " + READER_ADDRESS + ") values (?,?,?)";
    public static final String SQL_UPDATE = "update " + READER_TABLE + " set " + READER_NAME + " = ?, " + READER_SURNAME +" = ?, " + READER_ADDRESS + " = ? where " + READER_ID + " = ?";
    public static final String SQL_DELETE = "delete from " + READER_TABLE + " where " + READER_ID + " = ?";   
	
	List<Reader> findAll();
	Reader getById(int id);
	void insert(Reader reader);
	void update(Reader reader);
	boolean delete(int id);
}
