package by.epam.training.task_13.controller;

import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import by.epam.training.task_13.constants.ConstantsEroorsMsg;
import by.epam.training.task_13.constants.ConstantsJSP;
import by.epam.training.task_13.exeptions.CustomException;

@ControllerAdvice
public class ExceptionController {
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ModelAndView handleMissingParams(MissingServletRequestParameterException ex) {
        ModelAndView model = new ModelAndView("error/error");
        model.addObject(ConstantsJSP.KEY_ERROR_MESSAGE, ConstantsEroorsMsg.ERROR_NOT_CHOSEN_ANYONE 
        		+ ex.getParameterName().replaceAll("Id", ""));
        return model;
	}

	@ExceptionHandler(CustomException.class)
    public ModelAndView handleCustomException(CustomException e) {
        ModelAndView model = new ModelAndView("error/error");
        model.addObject(ConstantsJSP.KEY_ERROR_MESSAGE, e.getErrorMessage());
        return model;
    }
}
