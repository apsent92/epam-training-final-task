package by.epam.training.task_13.dao.rowmappers;

import static by.epam.training.task_13.dao.ConstantsColumnNames.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import by.epam.training.task_13.model.Reader;

public class ReaderMapper implements RowMapper<Reader>{

	public Reader mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Reader(rs.getInt(READER_ID), rs.getString(READER_NAME), 
				rs.getString(READER_SURNAME), rs.getString(READER_ADDRESS));
	}

}
