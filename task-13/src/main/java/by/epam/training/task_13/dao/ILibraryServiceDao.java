package by.epam.training.task_13.dao;

import static by.epam.training.task_13.dao.ConstantsColumnNames.*;
import java.sql.Date;
import java.util.List;
import by.epam.training.task_13.model.LibraryService;
import by.epam.training.task_13.model.Reader;

public interface ILibraryServiceDao {
	static final String POINT = ".";
	public static final String SQL_FIND_ALL = "select * from " + LIBRARY_SERVICE_TABLE + 
			" inner join " + READER_TABLE + " on " + LIBRARY_SERVICE_TABLE + POINT + LIBRARY_SERVICE_READER_ID + "=" +
			READER_TABLE + POINT + READER_ID + 			
			" inner join " + LITERATURE_TABLE + " on " + LIBRARY_SERVICE_TABLE + POINT + LIBRARY_SERVICE_LITERATURE_ID + "=" + 
			LITERATURE_TABLE + POINT + LITERATURE_ID + 
			" inner join " + BOOK_TABLE + " on " + LITERATURE_TABLE + POINT + LITERATURE_BOOK + " = " +
			BOOK_TABLE + POINT + BOOK_ID + 
			" inner join " + THEME_TABLE + " on " + LITERATURE_TABLE + POINT + LITERATURE_THEME + "=" + 
			THEME_TABLE + POINT + THEME_ID;
    
    public static final String SQL_FIND_BY_READER = SQL_FIND_ALL + 
    		" where (" + LIBRARY_SERVICE_READER_ID + " = ?)";
    
    public static final String SQL_FIND_BY_READER_ID_NOT_RETURNED = SQL_FIND_BY_READER + " and (" + 
    		LIBRARY_SERVICE_TABLE + POINT  + LIBRARY_SERVICE_DELIVERED +  "=false)" ;
    
    public static final String SQL_FIND_BY_READER_ID_RETURNED = SQL_FIND_BY_READER + " and (" + 
    		LIBRARY_SERVICE_TABLE + POINT  + LIBRARY_SERVICE_DELIVERED +  "=true)" ;
    
    public static final String SQL_INSERT = "insert into " + LIBRARY_SERVICE_TABLE + " (" + LIBRARY_SERVICE_LITERATURE_ID + ","
    		+ LIBRARY_SERVICE_READER_ID + "," + LIBRARY_SERVICE_DATE_ISSUE + "," + LIBRARY_SERVICE_DATE_RETURN + 
    		"," + LIBRARY_SERVICE_DELIVERED +") values (?,?,?,?,?)";
    
    public static final String SQL_CONFIRM_RETURN = "update " + LIBRARY_SERVICE_TABLE + 
    		" set " + LIBRARY_SERVICE_DELIVERED+" = true, " + LIBRARY_SERVICE_DATE_RETURN+" = ?" +
    		" where (" + LIBRARY_SERVICE_LITERATURE_ID + " = ?) and (" +
    		LIBRARY_SERVICE_READER_ID + " = ?) and (" + LIBRARY_SERVICE_DELIVERED + " = false)";

	List<LibraryService> findAll();
	List<LibraryService> findAllForReaderNotReturn(int id);
	List<LibraryService> findAllForReaderReturned(int id);
	void insert(LibraryService libraryService);
	void insertByLiteratureId(int literatureId, Reader reader, Date dateIsuue, Date dateReturn, boolean delivered);
	void confirmReturnByLiteratureId(Date dateReturn, int literatureId, Reader reader);
}
