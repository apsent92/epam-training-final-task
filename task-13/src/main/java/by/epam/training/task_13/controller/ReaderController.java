package by.epam.training.task_13.controller;

import java.util.List;
import static by.epam.training.task_13.constants.ConstantsJSP.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import by.epam.training.task_13.exeptions.CustomException;
import by.epam.training.task_13.model.Reader;
import by.epam.training.task_13.service.IReaderService;

@Controller
@RequestMapping("/readers")
public class ReaderController {

	IReaderService readerService;
	
	@Autowired
	public ReaderController(IReaderService readerService) {
		this.readerService = readerService;
	}

	@RequestMapping("")
	public String readers(Model model) {
		List<Reader> readers = readerService.findAll();
		model.addAttribute(KEY_READER_LIST, readers);
		return "readers_control";
	}

	@RequestMapping("/update")
	public String showUpdateForm(@RequestParam(KEY_READER_IDS) String[] ids, Model model) throws CustomException {
		try {
			Reader reader = readerService.getById(ids);
			model.addAttribute(KEY_READER_UPDATE_COMMAND, reader);
			return "reader/updateReader";
		} catch (CustomException e) {
			throw e;
		}
	}

	@RequestMapping(value = "/saveUpdates", method = RequestMethod.POST)
	public String upd(@ModelAttribute(KEY_READER) Reader reader) {
		readerService.update(reader);
		return "redirect:../readers";
	}

	@RequestMapping("/add")
	public String showAddForm(Model model) {
		model.addAttribute(KEY_READER_ADD_COMMAND, new Reader());
		return "reader/addReader";
	}

	@RequestMapping(value = "/saveNewReader")
	public String addreaders(@ModelAttribute(KEY_READER) Reader reader) {
		readerService.insert(reader);
		return "redirect:../readers";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(@RequestParam(KEY_READER_IDS) String[] ids) {
		readerService.delete(ids);
		return "redirect:../readers";
	}

}
