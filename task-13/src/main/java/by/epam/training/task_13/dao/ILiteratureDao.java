package by.epam.training.task_13.dao;

import static by.epam.training.task_13.dao.ConstantsColumnNames.*;
import java.util.List;
import by.epam.training.task_13.model.Literature;

public interface ILiteratureDao {
	static final String POINT = ".";
	public static final String SQL_FIND_ALL = "select * from " + LITERATURE_TABLE + " inner join " + BOOK_TABLE + " on " +
			LITERATURE_TABLE + POINT + LITERATURE_BOOK + " = " + BOOK_TABLE + POINT + BOOK_ID + " inner join " + THEME_TABLE +
			" on " + LITERATURE_TABLE + POINT + LITERATURE_THEME + "=" + THEME_TABLE + POINT + THEME_ID;
	
    public static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " where " + LITERATURE_ID + " = ?";
    
    public static final String SQL_FIND_ALL_AVAILABILITY = SQL_FIND_ALL + 
    		" where (" + LITERATURE_TABLE + POINT + LITERATURE_AVAILABILITY + "=true)";
    
    public static final String SQL_INSERT = "insert into " + LITERATURE_TABLE + " (" + LITERATURE_BOOK + ","
    		+ LITERATURE_THEME + "," + LITERATURE_COST + "," + LITERATURE_AVAILABILITY +") values (?,?,?,?)";
    
    public static final String SQL_MARK_LITERATURE_UNAVAILABILE = "update " + LITERATURE_TABLE + " set " + 
    		LITERATURE_AVAILABILITY + "=false where " + LITERATURE_ID + " = ?";
    
    public static final String SQL_MARK_LITERATURE_AVAILABILE = "update " + LITERATURE_TABLE + " set " + 
    		LITERATURE_AVAILABILITY + "=true where " + LITERATURE_ID + " = ?";
    
    public static final String SQL_UPDATE = "update " + LITERATURE_TABLE + " set " + LITERATURE_BOOK+" = ?,"
    		+ LITERATURE_THEME + " = ?," + LITERATURE_COST + " = ?," + LITERATURE_AVAILABILITY + "= ? where " + 
    		LITERATURE_ID + " = ?";
    
    public static final String SQL_DELETE = "delete from " + LITERATURE_TABLE + " where " + LITERATURE_ID + " = ?";   
	
	List<Literature> findAll();
	List<Literature> findAllAvailability();
	Literature getById(int id);
	void markLiteratureUnavailable(int literatureId);
	void markLiteratureAvailable(int literatureId);
	void insert(Literature literature);
	void update(Literature literature);
	boolean delete(int id);
}
