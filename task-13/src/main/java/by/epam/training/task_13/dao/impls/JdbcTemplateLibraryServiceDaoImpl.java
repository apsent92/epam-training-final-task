package by.epam.training.task_13.dao.impls;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import by.epam.training.task_13.dao.ILibraryServiceDao;
import by.epam.training.task_13.dao.rowmappers.LibraryServiceMapper;
import by.epam.training.task_13.model.LibraryService;
import by.epam.training.task_13.model.Reader;

@Repository
public class JdbcTemplateLibraryServiceDaoImpl implements ILibraryServiceDao{
	
    JdbcTemplate jdbcTemplate;
    
	@Autowired
	public JdbcTemplateLibraryServiceDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	@Override
	public List<LibraryService> findAll() {
		return jdbcTemplate.query(SQL_FIND_ALL, new LibraryServiceMapper());
	}
	@Override
	public void insert(LibraryService libraryService) {
		jdbcTemplate.update(SQL_INSERT, libraryService.getLiterature().getId(), libraryService.getReader().getId(),
				libraryService.getDateIssue(), libraryService.getDateReturn());
	}

	@Override
	public List<LibraryService> findAllForReaderNotReturn(int id) {
		return jdbcTemplate.query(SQL_FIND_BY_READER_ID_NOT_RETURNED, new LibraryServiceMapper(), id);
	}

	@Override
	public List<LibraryService> findAllForReaderReturned(int id) {
		return jdbcTemplate.query(SQL_FIND_BY_READER_ID_RETURNED, new LibraryServiceMapper(), id);
	}

	@Override
	public void insertByLiteratureId(int literatureId, Reader reader, Date dateIsuue, Date dateReturn, boolean delivered) {
		jdbcTemplate.update(SQL_INSERT, literatureId, reader.getId(), dateIsuue, dateReturn,delivered);	
	}

	@Override
	public void confirmReturnByLiteratureId(Date dateReturn, int literatureId, Reader reader) {
		jdbcTemplate.update(SQL_CONFIRM_RETURN, dateReturn, literatureId, reader.getId());
		
	}
}
