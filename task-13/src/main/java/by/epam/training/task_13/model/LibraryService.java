package by.epam.training.task_13.model;

import java.sql.Date;

public class LibraryService {
	private Literature literature;
	private Reader reader;
	private Date dateIssue;
	private Date dateReturn;
	private boolean delivered;
	public LibraryService() {
		super();
	}
	public LibraryService(Literature literature, Reader reader, Date dateIssue, Date dateReturn, boolean delivered) {
		super();
		this.literature = literature;
		this.reader = reader;
		this.dateIssue = dateIssue;
		this.dateReturn = dateReturn;
		this.delivered = delivered;
	}
	public boolean getDelivered() {
		return delivered;
	}
	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}
	public Literature getLiterature() {
		return literature;
	}
	public void setLiterature(Literature literature) {
		this.literature = literature;
	}
	public Reader getReader() {
		return reader;
	}
	public void setReader(Reader reader) {
		this.reader = reader;
	}
	public Date getDateIssue() {
		return dateIssue;
	}
	public void setDateIssue(Date dateIssue) {
		this.dateIssue = dateIssue;
	}
	public Date getDateReturn() {
		return dateReturn;
	}
	public void setDateReturn(Date dateReturn) {
		this.dateReturn = dateReturn;
	}
	@Override
	public String toString() {
		return "[literature=" + literature + ", reader=" + reader + ", dateIssue=" + dateIssue
				+ ", dateReturn=" + dateReturn + "]";
	}
}
