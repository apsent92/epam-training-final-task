package by.epam.training.task_13.constants;

public class ConstantsJSP {
	public static final String KEY_READER_LIST="readerList";
	public static final String KEY_READER="reader";
	public static final String KEY_READER_ID = "id";
	public static final String KEY_READER_NAME = "name";
	public static final String KEY_READER_SURNAME = "surname";
	public static final String KEY_READER_ADDRESS = "address";
	public static final String KEY_READER_UPDATE_COMMAND="command";
	public static final String KEY_READER_ADD_COMMAND="command";
	public static final String KEY_READER_IDS="readerIds";
	public static final String KEY_COOSE_READER_RADIO="readerId";
	public static final String KEY_LIBRARY_SERVICE_READER="reader";
	public static final String KEY_LITERATURE_IDS="literatureIds";
	public static final String KEY_ERROR_MESSAGE="errMsg";
}
