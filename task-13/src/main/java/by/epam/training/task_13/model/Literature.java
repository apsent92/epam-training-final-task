package by.epam.training.task_13.model;

public class Literature {
	private int id;
	private Book book;
	private Theme theme;
	private double cost;
	private boolean availability;
	public Literature() {
		super();
	}
	public Literature(int id, Book book, Theme theme, double cost, boolean availability) {
		super();
		this.id = id;
		this.book = book;
		this.theme = theme;
		this.cost = cost;
		this.availability = availability;
	}
	public boolean getAvailability() {
		return availability;
	}

	public void setAvailability(boolean availability) {
		this.availability = availability;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public Theme getTheme() {
		return theme;
	}
	public void setTheme(Theme theme) {
		this.theme = theme;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	@Override
	public String toString() {
		return "Literature [id=" + id + ", book=" + book + ", theme=" + theme + ", cost=" + cost + ", availability="
				+ availability + "]";
	}
}
