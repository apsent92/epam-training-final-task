package by.epam.training.task_13.service.impl;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.training.task_13.dao.ILibraryServiceDao;
import by.epam.training.task_13.model.Reader;
import by.epam.training.task_13.service.ILibrarySeviceService;

import static by.epam.training.task_13.utils.Utils.*;
import by.epam.training.task_13.model.LibraryService;
import by.epam.training.task_13.model.Literature;;

@Service
public class LibraryServiceServiceImpl implements ILibrarySeviceService {

	ILibraryServiceDao libraryServiceDao;
	
	@Autowired
	public LibraryServiceServiceImpl(ILibraryServiceDao libraryServiceDao) {
		super();
		this.libraryServiceDao = libraryServiceDao;
	}

	@Override
	public List<Literature> findAllLitForReaderNotReturn(Reader reader) {
		List<LibraryService> libraryServices = libraryServiceDao.findAllForReaderNotReturn(reader.getId());
		List<Literature> literature = libraryServices.stream().map(o -> o.getLiterature()).collect(Collectors.toList());
		return literature;
	}

	@Override
	public List<LibraryService> findAllLitForReaderReturned(Reader reader) {
		return libraryServiceDao.findAllForReaderReturned(reader.getId());
	}

	@Override
	public void addLiteratureToReader(String[] literatureIds, Reader reader) {
		int[] ids = getIds(literatureIds);
		Date dateIssue = new Date(System.currentTimeMillis());
		Arrays.stream(ids).forEach(id -> libraryServiceDao.insertByLiteratureId(id, reader, dateIssue, null, false));
	}

	@Override
	public void confirmReturnReader(String[] literatureIds, Reader reader) {
		int[] ids = getIds(literatureIds);
		Date dateReturn = new Date(System.currentTimeMillis());
		Arrays.stream(ids).forEach(id -> libraryServiceDao.confirmReturnByLiteratureId(dateReturn, id, reader));
	}
}
