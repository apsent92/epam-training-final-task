package by.epam.training.task_13.exeptions;

public class CustomException extends RuntimeException{
	
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}
	public CustomException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}
	public CustomException() {
		super();
	}
	public CustomException(String errorMessage, Throwable cause) {
		super(cause);
		this.errorMessage = errorMessage;
	}
	public CustomException(Throwable cause) {
		super(cause);
	}
}
