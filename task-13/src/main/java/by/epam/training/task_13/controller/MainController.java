package by.epam.training.task_13.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import by.epam.training.task_13.constants.ConstantsJSP;
import by.epam.training.task_13.model.Reader;
import by.epam.training.task_13.service.IReaderService;

@Controller
public class MainController {
	
	IReaderService readerService;
	
	@Autowired
	public MainController(IReaderService readerService) {
		this.readerService = readerService;
	}

	@RequestMapping("/")
    public String getIndex(){
        return "/index";
    }
	
	@RequestMapping("chooseReader")
    public String chooseReader(Model model){
		List<Reader> readers = readerService.findAll();
		model.addAttribute(ConstantsJSP.KEY_READER_LIST, readers);
		return "choose_reader";
	}
	
}
