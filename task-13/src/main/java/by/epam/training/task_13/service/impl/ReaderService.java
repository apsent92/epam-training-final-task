package by.epam.training.task_13.service.impl;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import by.epam.training.task_13.dao.IReaderDao;
import by.epam.training.task_13.exeptions.CustomException;
import by.epam.training.task_13.model.Reader;
import by.epam.training.task_13.service.IReaderService;

import static by.epam.training.task_13.utils.Utils.*;

@Service
public class ReaderService implements IReaderService {

	IReaderDao readerDao;
	
	@Autowired
	public ReaderService(IReaderDao readerDao) {
		this.readerDao = readerDao;
	}

	@Override
	public List<Reader> findAll() {
		return readerDao.findAll();
	}

	@Override
	public Reader getById(int id) {
		if (id > 0) {
			return readerDao.getById(id);
		}
		return null;
	}

	@Override
	public Reader getById(String[] ids) throws CustomException {
		try {
			checkInputCheckboxesForUpdating(ids);
			int id = Integer.parseInt(ids[0]);
			return getById(id);
		} catch (CustomException e) {
			throw e;
		}
	}

	@Override
	public void insert(Reader reader) {
		if (reader != null) {
			readerDao.insert(reader);
		}
	}

	@Override
	public void update(Reader reader) {
		if (reader != null) {
			readerDao.update(reader);
		}
	}

	@Override
	public void delete(String[] ids) throws CustomException {
		int[] listIds = getIds(ids);
		Arrays.stream(listIds).forEach(id -> readerDao.delete(id));
	}
}
