package by.epam.training.task_13.dao.rowmappers;

import static by.epam.training.task_13.dao.ConstantsColumnNames.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import by.epam.training.task_13.model.LibraryService;

public class LibraryServiceMapper implements RowMapper<LibraryService>{

	public LibraryService mapRow(ResultSet rs, int rowNum) throws SQLException {
		LibraryService libraryService = new LibraryService();
		libraryService.setLiterature(new LiteratureMapper().mapRow(rs, rowNum));
		libraryService.setReader(new ReaderMapper().mapRow(rs, rowNum));
		libraryService.setDateIssue(rs.getDate(LIBRARY_SERVICE_DATE_ISSUE));
		libraryService.setDateReturn(rs.getDate(LIBRARY_SERVICE_DATE_RETURN));
		libraryService.setDelivered(rs.getBoolean(LIBRARY_SERVICE_DELIVERED));
		return libraryService;
	}

}
