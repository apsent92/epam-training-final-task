package by.epam.training.task_13.service.impl;

import static by.epam.training.task_13.utils.Utils.*;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.training.task_13.dao.ILiteratureDao;
import by.epam.training.task_13.model.Literature;
import by.epam.training.task_13.service.ILiteratureService;

@Service
public class LiteratureServiceImpl implements ILiteratureService{
	
	ILiteratureDao literatureDao;
	
	@Autowired
	public LiteratureServiceImpl(ILiteratureDao literatureDao) {
		this.literatureDao = literatureDao;
	}

	@Override
	public List<Literature> findAllAvailabilityLiterature() {
		return literatureDao.findAllAvailability();
	}

	@Override
	public void markLiteratureUnavailable(String[] literatureIds) {
		int[] ids = getIds(literatureIds);
		Arrays.stream(ids).forEach(id -> literatureDao.markLiteratureUnavailable(id));
	}

	@Override
	public void markLiteratureAvailable(String[] literatureIds) {
		int[] ids = getIds(literatureIds);
		Arrays.stream(ids).forEach(id -> literatureDao.markLiteratureAvailable(id));
	}
}
