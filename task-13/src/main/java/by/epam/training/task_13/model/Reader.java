package by.epam.training.task_13.model;

public class Reader {
	private int id;
	private String name;
	private String surname;
	private String address;
	public Reader() {
		super();
	}
	public Reader(int id, String name, String surname, String address) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.address = address;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return id + ";" + name + ";" + surname + ";" + address;
	}
}
