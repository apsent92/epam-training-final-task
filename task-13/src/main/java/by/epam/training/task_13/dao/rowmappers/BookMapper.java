package by.epam.training.task_13.dao.rowmappers;


import static by.epam.training.task_13.dao.ConstantsColumnNames.*;

import java.sql.ResultSet;

import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import by.epam.training.task_13.model.Book;

public class BookMapper implements RowMapper<Book>{

	public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Book(rs.getInt(BOOK_ID), rs.getString(BOOK_NAME));
	}

}
