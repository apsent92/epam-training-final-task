package by.epam.training.task_13.dao.impls;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import by.epam.training.task_13.dao.IBookDao;
import by.epam.training.task_13.dao.rowmappers.BookMapper;
import by.epam.training.task_13.model.Book;

@Repository
public class JdbcTemplateBookDaoImpl implements IBookDao{
	
    JdbcTemplate jdbcTemplate;
    
    @Autowired
	public JdbcTemplateBookDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Book> findAll() {
		return jdbcTemplate.query(SQL_FIND_ALL, new BookMapper());
	}

	public Book getById(int id) {
		return jdbcTemplate.queryForObject(SQL_FIND_BY_ID, new BookMapper(), id);
	}

	public void insert(Book book) {
		jdbcTemplate.update(SQL_INSERT, book.getName());
	}

	public void update(Book book) {
		jdbcTemplate.update(SQL_UPDATE, book.getName(), book.getId());
	}

	public boolean delete(int id) {
		return jdbcTemplate.update(SQL_DELETE, id) > 0;
	}
}
