package by.epam.training.task_13.dao.rowmappers;

import static by.epam.training.task_13.dao.ConstantsColumnNames.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import by.epam.training.task_13.model.Theme;

public class ThemeMapper implements RowMapper<Theme>{

	public Theme mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Theme(rs.getInt(THEME_ID), rs.getString(THEME_NAME));
	}

}
