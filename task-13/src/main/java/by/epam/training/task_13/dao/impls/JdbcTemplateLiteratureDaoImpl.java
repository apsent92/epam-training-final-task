package by.epam.training.task_13.dao.impls;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import by.epam.training.task_13.dao.ILiteratureDao;
import by.epam.training.task_13.dao.rowmappers.LiteratureMapper;
import by.epam.training.task_13.model.Literature;

@Repository
public class JdbcTemplateLiteratureDaoImpl implements ILiteratureDao{
	
    JdbcTemplate jdbcTemplate;
    
	@Autowired
	public JdbcTemplateLiteratureDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Literature> findAll() {
		return jdbcTemplate.query(SQL_FIND_ALL, new LiteratureMapper());
	}

	public Literature getById(int id) {
		return jdbcTemplate.queryForObject(SQL_FIND_BY_ID, new LiteratureMapper(), id);
	}

	public void insert(Literature literature) {
		jdbcTemplate.update(SQL_INSERT, literature.getBook().getId(), literature.getTheme().getId(),
				literature.getCost(), literature.getAvailability());
	}

	public void update(Literature literature) {
		jdbcTemplate.update(SQL_UPDATE, literature.getBook().getId(), literature.getTheme().getId(), 
				literature.getCost(), literature.getAvailability(), literature.getId());
	}

	public boolean delete(int id) {
		return jdbcTemplate.update(SQL_DELETE, id) > 0;
	}

	@Override
	public List<Literature> findAllAvailability() {
		return jdbcTemplate.query(SQL_FIND_ALL_AVAILABILITY, new LiteratureMapper());
	}

	@Override
	public void markLiteratureUnavailable(int literatureId) {
		jdbcTemplate.update(SQL_MARK_LITERATURE_UNAVAILABILE,literatureId);	
	}

	@Override
	public void markLiteratureAvailable(int literatureId) {
		jdbcTemplate.update(SQL_MARK_LITERATURE_AVAILABILE,literatureId);
		
	}
}
