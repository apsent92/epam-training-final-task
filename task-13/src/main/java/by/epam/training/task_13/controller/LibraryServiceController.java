package by.epam.training.task_13.controller;

import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import by.epam.training.task_13.model.LibraryService;
import by.epam.training.task_13.model.Literature;
import by.epam.training.task_13.model.Reader;
import by.epam.training.task_13.service.ILibrarySeviceService;
import by.epam.training.task_13.service.ILiteratureService;
import by.epam.training.task_13.service.IReaderService;
import static by.epam.training.task_13.constants.ConstantsJSP.*;

@Controller
@RequestMapping("/service")
public class LibraryServiceController {

	ILibrarySeviceService libraryServiceService;
	IReaderService readerService;
	ILiteratureService literatureService;
	
	
	@Autowired
	public LibraryServiceController(ILibrarySeviceService libraryServiceService, IReaderService readerService,
			ILiteratureService literatureService) {
		this.libraryServiceService = libraryServiceService;
		this.readerService = readerService;
		this.literatureService = literatureService;
	}

	@RequestMapping("")
	public String showReadersLiterature(Model model, HttpSession httpSession) {
		Reader reader = (Reader) httpSession.getAttribute(KEY_LIBRARY_SERVICE_READER);
		List<Literature> literatures = libraryServiceService.findAllLitForReaderNotReturn(reader);
		model.addAttribute("literatureList", literatures);
		return "service_control";
	}

	@RequestMapping("/setReader")
	public String showLiteratureAcrossId(@RequestParam(KEY_COOSE_READER_RADIO) String[] ids, Model model, HttpSession httpSession) {
		Reader reader = readerService.getById(ids);
		httpSession.setAttribute(KEY_LIBRARY_SERVICE_READER, reader);
		return "redirect:../service";
	}

	@RequestMapping("/formAddLiterature")
	public String formAddLiteratureToReaders(Model model, HttpSession httpSession) {
		List<Literature> literatures = literatureService.findAllAvailabilityLiterature();
		model.addAttribute("literatureList", literatures);
		return "service/add_lit_to_reader";
	}

	@RequestMapping(value = "/addLiterature", method = RequestMethod.POST)
	public String addLiteratureToReaders(@RequestParam(KEY_LITERATURE_IDS) String[] literatureIds, Model model,
			HttpSession httpSession) {
		Reader reader = (Reader) httpSession.getAttribute(KEY_LIBRARY_SERVICE_READER);
		libraryServiceService.addLiteratureToReader(literatureIds, reader);
		literatureService.markLiteratureUnavailable(literatureIds);
		return "redirect:../service";
	}

	@RequestMapping("/showReturnedLit")
	public String showReturnedLiteratureToReaders(Model model, HttpSession httpSession) {
		Reader reader = (Reader) httpSession.getAttribute(KEY_LIBRARY_SERVICE_READER);
		List<LibraryService> libraryServices = libraryServiceService.findAllLitForReaderReturned(reader);
		model.addAttribute("libraryServicesList", libraryServices);
		return "service/show_returned_lit";
	}

	@RequestMapping("/confirm")
	public String confirmReturn(@RequestParam(KEY_LITERATURE_IDS) String[] literatureIds, Model model, HttpSession httpSession) {
		Reader reader = (Reader) httpSession.getAttribute(KEY_LIBRARY_SERVICE_READER);
		libraryServiceService.confirmReturnReader(literatureIds, reader);
		literatureService.markLiteratureAvailable(literatureIds);
		return "redirect:../service";
	}

}
