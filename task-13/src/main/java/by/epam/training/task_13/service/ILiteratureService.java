package by.epam.training.task_13.service;

import java.util.List;

import by.epam.training.task_13.model.Literature;

public interface ILiteratureService {
	List<Literature> findAllAvailabilityLiterature();
	void markLiteratureUnavailable(String[] literatureIds);
	void markLiteratureAvailable(String[] literatureIds);
}
