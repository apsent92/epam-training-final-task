package by.epam.training.task_13.dao;

import java.util.List;

import by.epam.training.task_13.model.Book;
import static by.epam.training.task_13.dao.ConstantsColumnNames.*;

public interface IBookDao {
	public static final String SQL_FIND_ALL = "select * from " + BOOK_TABLE;
    public static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " where " + BOOK_ID + " = ?";
    public static final String SQL_INSERT = "insert into " + BOOK_TABLE + " (" + BOOK_NAME + ") values (?)";
    public static final String SQL_UPDATE = "update " + BOOK_TABLE + " set " + BOOK_NAME +" = ? where " + BOOK_ID + " = ?";
    public static final String SQL_DELETE = "delete from " + BOOK_TABLE + " where " + BOOK_ID + " = ?";   
	
	List<Book> findAll();
	Book getById(int id);
	void insert(Book book);
	void update(Book book);
	boolean delete(int id);

}
