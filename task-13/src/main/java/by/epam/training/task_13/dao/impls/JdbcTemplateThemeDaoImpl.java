package by.epam.training.task_13.dao.impls;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import by.epam.training.task_13.dao.IThemeDao;
import by.epam.training.task_13.dao.rowmappers.ThemeMapper;
import by.epam.training.task_13.model.Theme;

@Repository
public class JdbcTemplateThemeDaoImpl implements IThemeDao{
	
    JdbcTemplate jdbcTemplate;
    
	@Autowired
	public JdbcTemplateThemeDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Theme> findAll() {
		return jdbcTemplate.query(SQL_FIND_ALL, new ThemeMapper());
	}

	public Theme getById(int id) {
		return jdbcTemplate.queryForObject(SQL_FIND_BY_ID, new ThemeMapper(), id);
	}

	public void insert(Theme theme) {
		jdbcTemplate.update(SQL_INSERT, theme.getName());
	}

	public void update(Theme theme) {
		jdbcTemplate.update(SQL_UPDATE, theme.getName(), theme.getId());
	}

	public boolean delete(int id) {
		return jdbcTemplate.update(SQL_DELETE, id) > 0;
	}

}
