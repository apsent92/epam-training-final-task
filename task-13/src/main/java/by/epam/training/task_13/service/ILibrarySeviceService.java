package by.epam.training.task_13.service;

import java.util.List;

import by.epam.training.task_13.model.LibraryService;
import by.epam.training.task_13.model.Literature;
import by.epam.training.task_13.model.Reader;

public interface ILibrarySeviceService {
	List<Literature> findAllLitForReaderNotReturn(Reader reader);
	List<LibraryService> findAllLitForReaderReturned(Reader reader);
	void addLiteratureToReader(String[] literatureIds, Reader reader);
	void confirmReturnReader(String[] literatureIds, Reader reader);
}
