package by.epam.training.task_13.service;

import java.util.List;

import by.epam.training.task_13.exeptions.CustomException;
import by.epam.training.task_13.model.Reader;

public interface IReaderService {
	List<Reader> findAll();
	Reader getById(int id);
	public Reader getById(String[] ids) throws CustomException;
	void insert(Reader reader);
	void update(Reader reader);
	void delete(String[] ids);
}
