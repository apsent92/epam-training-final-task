package by.epam.training.task_13.dao.impls;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import by.epam.training.task_13.dao.IReaderDao;
import by.epam.training.task_13.dao.rowmappers.ReaderMapper;
import by.epam.training.task_13.model.Reader;

@Repository
public class JdbcTemplateReaderDaoImpl implements IReaderDao{
	
    JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcTemplateReaderDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Reader> findAll() {
		return jdbcTemplate.query(SQL_FIND_ALL, new ReaderMapper());
	}

	public Reader getById(int id) {
		return jdbcTemplate.queryForObject(SQL_FIND_BY_ID, new ReaderMapper(), id);
	}

	public void insert(Reader reader) {
		jdbcTemplate.update(SQL_INSERT, reader.getName(), reader.getSurname(), reader.getAddress());
	}

	public void update(Reader reader) {
		jdbcTemplate.update(SQL_UPDATE, reader.getName(), reader.getSurname(), reader.getAddress(), reader.getId());
	}

	public boolean delete(int id) {
		return jdbcTemplate.update(SQL_DELETE, id) > 0;
	}

}
