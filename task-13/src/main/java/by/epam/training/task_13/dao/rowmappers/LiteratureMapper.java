package by.epam.training.task_13.dao.rowmappers;

import static by.epam.training.task_13.dao.ConstantsColumnNames.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import by.epam.training.task_13.model.Literature;

public class LiteratureMapper implements RowMapper<Literature>{

	public Literature mapRow(ResultSet rs, int rowNum) throws SQLException {
		Literature literature = new Literature();
		literature.setId(rs.getInt(LITERATURE_ID));
		literature.setBook(new BookMapper().mapRow(rs, rowNum));
		literature.setTheme(new ThemeMapper().mapRow(rs, rowNum));
		literature.setCost(rs.getDouble(LITERATURE_COST));
		literature.setAvailability(rs.getBoolean(LITERATURE_AVAILABILITY));
		return literature;
	}

}
