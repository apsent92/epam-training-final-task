package by.epam.training.task_13.dao;

public class ConstantsColumnNames {
	
	public static String BOOK_TABLE = "books";
	public static String BOOK_ID = "bookId";
	public static String BOOK_NAME = "book";
	
	public static String THEME_TABLE = "themes";
	public static String THEME_ID = "themeId";
	public static String THEME_NAME = "theme";
	
	public static String READER_TABLE = "readers";
	public static String READER_ID = "readerId";
	public static String READER_NAME = "name";
	public static String READER_SURNAME = "surname";
	public static String READER_ADDRESS = "address";
	
	public static String LITERATURE_TABLE = "literature";
	public static String LITERATURE_ID = "literatureId";
	public static String LITERATURE_BOOK = "idBook";
	public static String LITERATURE_THEME = "idTheme";
	public static String LITERATURE_COST = "cost";
	public static String LITERATURE_AVAILABILITY = "availability";
	
	public static String LIBRARY_SERVICE_TABLE = "library_service";
	public static String LIBRARY_SERVICE_LITERATURE_ID = "idLiterature";
	public static String LIBRARY_SERVICE_READER_ID = "idReader";
	public static String LIBRARY_SERVICE_DATE_ISSUE = "dateIssue";
	public static String LIBRARY_SERVICE_DATE_RETURN = "dateReturn";
	public static String LIBRARY_SERVICE_DELIVERED = "delivered";
}
