package by.epam.training.task_13.utils;

import java.util.Arrays;
import by.epam.training.task_13.constants.ConstantsEroorsMsg;
import by.epam.training.task_13.exeptions.CustomException;

public class Utils {
	public static int[] getIds(String[] ids) {
		int[] tasksId = Arrays.stream(ids).mapToInt(Integer::parseInt).toArray();
		return tasksId;
	}

	public static void checkInputCheckboxesForUpdating(String[] ids) throws CustomException {
		if (ids.length != 1) {
			throw new CustomException(ConstantsEroorsMsg.ERROR_MORE_THAN_ONE_ID);
		}
	}

}
