package by.epam.training.task_13.dao;

import static by.epam.training.task_13.dao.ConstantsColumnNames.*;
import java.util.List;
import by.epam.training.task_13.model.Theme;

public interface IThemeDao {
	public static final String SQL_FIND_ALL = "select * from " + THEME_TABLE;
    public static final String SQL_FIND_BY_ID = SQL_FIND_ALL + " where " + THEME_ID + " = ?";
    public static final String SQL_INSERT = "insert into " + THEME_TABLE + " (" + THEME_NAME + ") values (?)";
    public static final String SQL_UPDATE = "update " + THEME_TABLE + " set " + THEME_NAME +" = ? where " + THEME_ID + " = ?";
    public static final String SQL_DELETE = "delete from " + THEME_TABLE + " where " + THEME_ID + " = ?";   

	List<Theme> findAll();
	Theme getById(int id);
	void insert(Theme theme);
	void update(Theme theme);
	boolean delete(int id);
}
