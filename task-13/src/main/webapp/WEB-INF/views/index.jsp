<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html lang="en">
<head>
<style type="text/css">
    <%@include file="style/style.css" %>
</style>
<meta charset="ISO-8859-1">
<title>Main page</title>
</head>
<body>
	<jsp:include page="includ/header.jsp" flush="true" />
	
	<hr />
	<table class="main_page">
		<tr>
			<td ><a href="readers" class="button">Go to control readers</a></td>
		</tr>
	</table>
		
	<hr />
	<table class="main_page">
		<tr>
			<td ><a href="chooseReader"  class="button">Go to library service</a></td>
		</tr>
	</table>
		
	<jsp:include page="includ/footer.jsp" flush="true" />
</body>
</html>