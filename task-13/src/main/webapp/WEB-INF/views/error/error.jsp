<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style type="text/css">
    <%@include file="../style/style.css" %>
</style>
<script type="text/javascript">
    <%@include file="../script/script.js" %>
</script>
<title>Error page</title>
</head>
<body>
	<jsp:include page="../includ/header.jsp" flush="true" />

	<table align="center" wight="100%" class="main_page">
		<tr>
			<td>Spring MVC @ControllerAdvice Exception</td>
		</tr>
		<tr>
			<td>Your error: <c:if test="${not empty errMsg}">${errMsg}</c:if></td>
			
		</tr>
	</table>
	<hr />
	<table align="center" wight="100%" class="main_page">
		<tr>
			<td><a onClick="javascript:goBack()" class="button">Go back</a></td>
		</tr>
	</table>
	<jsp:include page="../includ/footer.jsp" flush="true" />
</body>
</html>