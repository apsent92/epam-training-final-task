<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="by.epam.training.task_13.constants.ConstantsJSP" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style type="text/css">
    <%@include file="style/style.css" %>
</style>
<title>Insert title here</title>
</head>
<script type="text/javascript">
    <%@include file="script/script.js" %>
</script>
<body>
	<jsp:include page="includ/header.jsp" flush="true" />
	<table class="descript">
		<tr>
			<td >Choose reader</td>
		</tr>
	</table>
	<hr />
	<form name="readersForm" method="get">
		<table border="1" width="100%" align="center" class="table_style">
			<tr>
				<th></th>
				<th>Name</th>
				<th>Surname</th>
				<th>Address</th>
			</tr>
			<c:forEach var="reader" items="${readerList}">
				<tr>
					<td><INPUT type="radio" name="<%= ConstantsJSP.KEY_COOSE_READER_RADIO %>" value="${reader.id}"></td>
					<td><c:out value="${reader.name}" /></td>
					<td><c:out value="${reader.surname}" /></td>
					<td><c:out value="${reader.address}" /></td>
				</tr>
			</c:forEach>
		</table>
	</form>

<hr/>
	<table align="center" wight="100%">
		<tr>
			<td><a onClick="javascript:control()" class="button">Control library
					service</a></td>
					<td><a href="./" class="button">Go to main</a></td>
		</tr>
	</table>
	<jsp:include page="includ/footer.jsp" flush="true"/>

</body>
</html>