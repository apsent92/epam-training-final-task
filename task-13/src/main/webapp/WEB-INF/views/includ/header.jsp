<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div>
	<hr />
	<table align="center" wight="100%" class="header">
		<tr>
			<td text-align="right">Library management service</td>
			<td text-align="right">Your name: <b><c:out
						value="${pageContext.request.remoteUser}" /></td>
			<td text-align="left">		
				<form name="logout" action="./logout" method="post">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" /> <a onClick="document.logout.submit()"
						class="button"> Logout</a>
				</form>
			</td>
		</tr>
	</table>
	<hr />
</div>