<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Control readers</title>
</head>
<style type="text/css">
    <%@include file="style/style.css" %>
</style>
<title>Insert title here</title>
</head>
<script type="text/javascript">
    <%@include file="script/script.js" %>
</script>
<body>
	<jsp:include page="includ/header.jsp" flush="true" />
	<table class="descript">
		<tr>
			<td >Control readers</td>
		</tr>
	</table>
	<hr/>
	<jsp:include page="reader/readers.jsp" flush="true" />
	<hr />
	<table align="center" wight="100%">
		<tr>
			<td><a href="readers/add" class="button">Add reader</a></td>
			<td><a onClick="javascript:upd()" class="button">Update reader</a></td>
			<td><a onClick="javascript:del()" class="button">Delete reader/readers</a></td>
			<td><a href="./" class="button">Go to main</a></td>

		</tr>
	</table>
	<jsp:include page="includ/footer.jsp" flush="true"/>
</body>
</html>