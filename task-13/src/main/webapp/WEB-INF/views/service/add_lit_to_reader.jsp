<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add literature to reader</title>
</head>
<style type="text/css">
    <%@include file="../style/style.css" %>
</style>
<script type="text/javascript">
    <%@include file="../script/script.js" %>
</script>
<body>
	<jsp:include page="../includ/header.jsp" flush="true" />
	<table class="descript">
		<tr>
			<td >Literature in stock</td>
		</tr>
	</table>
	<hr />
<jsp:include page="literature.jsp" flush="true" />
<hr />
	<table align="center" wjght="100%">
		<tr>
			<td><a onClick="addLiterature()" class="button">Add choosen literature</a></td>
			<td><a onClick="javascript:goBack()" class="button">Back to service</a></td>

		</tr>
	</table>
 	<jsp:include page="../includ/footer.jsp" flush="true" />
</body>
</html>