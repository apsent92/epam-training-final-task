<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style type="text/css">
    <%@include file="../style/style.css" %>
</style>
<script type="text/javascript">
    <%@include file="../script/script.js" %>
</script>
<title>Reader's returned literature</title>
</head>
<body>
	<jsp:include page="../includ/header.jsp" flush="true" />
	<table class="descript">
		<tr>
			<td ><c:out value="${reader.name}" /> <c:out value="${reader.surname}" />'s literature was returned</td>
		</tr>
	</table>

	<hr/>
	<jsp:include page="library_services.jsp" flush="true" />
	<jsp:include page="../includ/footer.jsp" flush="true" />
</body>
</html>