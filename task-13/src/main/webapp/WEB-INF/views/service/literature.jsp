<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="by.epam.training.task_13.constants.ConstantsJSP" %>
<form name="literatureForm" method="get">
	<table border="1" width="100%" align="center" class="table_style">
		<tr>
			<th></th>
			<th>Book name</th>
			<th>Theme name</th>
			<th>Cost</th>
		</tr>
		<c:forEach var="lit" items="${literatureList}">
			<tr>
				<td><INPUT name="<%= ConstantsJSP.KEY_LITERATURE_IDS %>" type=checkbox value="${lit.id}"></td>
				<td><c:out value="${lit.book.name}" /></td>
				<td><c:out value="${lit.theme.name}" /></td>
				<td><c:out value="${lit.cost}" /></td>

			</tr>
		</c:forEach>
	</table>
</form>
