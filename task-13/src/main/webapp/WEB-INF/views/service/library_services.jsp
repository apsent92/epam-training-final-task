<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table border="1" width="100%" align="center" class="table_style">
	<tr>
		<th>Book name</th>
		<th>Theme name</th>
		<th>Date issue</th>
		<th>Date return</th>
	</tr>
	<c:forEach var="library" items="${libraryServicesList}">
		<tr>
			<td><c:out value="${library.literature.book.name}" /></td>
			<td><c:out value="${library.literature.theme.name}" /></td>
			<td><c:out value="${library.dateIssue}" /></td>
			<td><c:out value="${library.dateReturn}" /></td>
		</tr>
	</c:forEach>
</table>

<hr />
	<table align="center" wight="100%">
		<tr>
			<td><a onClick="javascript:goBack()" class="button">Back to service</a></td>
		</tr>
	</table>
