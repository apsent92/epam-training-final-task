<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Library service</title>
<style type="text/css">
	<%@include file="style/style.css"%>
</style>
<script type="text/javascript">
	<%@include file="script/script.js" %>	
</script>
</head>

<body>
	<jsp:include page="includ/header.jsp" flush="true" />
	<table class="descript">
		<tr>
			<td ><c:out value="${reader.name}" /> <c:out	value="${reader.surname}" /> 's literature not return</td>
		</tr>
	</table>
	<hr />
	<jsp:include page="service/literature.jsp" flush="true" />
	<hr />
	<table align="center" wight="100%" >
		<tr>
			<td><a onClick="javascript:showReturned()" class="button">Show
					returned literature</a></td>
			<td><a href="service/formAddLiterature" class="button">Add
					literature</a></td>
			<td><a onClick="javascript:confirm()" class="button">Confirm
					return</a></td>
			<td><a href="./chooseReader" class="button">Back to list of
					readers</a></td>
		</tr>
	</table>
	</form>
	<jsp:include page="includ/footer.jsp" flush="true" />
</body>
</html>