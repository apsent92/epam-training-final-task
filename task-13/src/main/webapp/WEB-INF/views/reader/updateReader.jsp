<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="by.epam.training.task_13.constants.ConstantsJSP" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update reader</title>
<style type="text/css">
    <%@include file="../style/style.css" %>
</style>
<script type="text/javascript">
    <%@include file="../script/script.js" %>
</script>
</head>
<body>
	<jsp:include page="../includ/header.jsp" flush="true" />
	<form:form name="updateReader" method="post" action="../readers/saveUpdates">
		<table align="center" wjght="100%" class="table_style">
			<tr>
				<th></th>
				<th>Update reader</th>
			</tr>
			<tr>
				<td></td>
				<td><form:hidden path="<%= ConstantsJSP.KEY_READER_ID %>" /></td>
			</tr>
			<tr>
				<td>Name:</td>
				<td><form:input path="<%= ConstantsJSP.KEY_READER_NAME %>" /></td>
			</tr>
			<tr>
				<td>Surname: </td>
				<td><form:input path="<%= ConstantsJSP.KEY_READER_SURNAME %>" /></td>
			</tr>
			<tr>
				<td>Address: </td>
				<td><form:input path="<%= ConstantsJSP.KEY_READER_ADDRESS %>" /></td>
			</tr>
		</table>
	</form:form>
		<hr/>
	<table align="center" wight="100%">
		<tr>
			<td><a onclick="document.updateReader.submit()" class="button">Save updates</a></td>
			<td><a onclick="javascript:goBack()" class="button">Go back</a></td>
		</tr>
	</table>
	<jsp:include page="../includ/footer.jsp" flush="true" />
</body>
</html>