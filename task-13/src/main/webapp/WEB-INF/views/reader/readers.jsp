<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="by.epam.training.task_13.constants.ConstantsJSP" %>
<form name="readersForm" method="get">
	<table border="1" width="100%" align="center" class="table_style">
		<tr>
			<th></th>
			<th>Name</th>
			<th>Surname</th>
			<th>Address</th>
		</tr>
		<c:forEach var="reader" items="${readerList}">
			<tr>
				<td><INPUT name="<%= ConstantsJSP.KEY_READER_IDS %>" type=checkbox value="${reader.id}"></td>
				<td><c:out value="${reader.name}" /></td>
				<td><c:out value="${reader.surname}" /></td>
				<td><c:out value="${reader.address}" /></td>
			</tr>
		</c:forEach>
	</table>
</form>